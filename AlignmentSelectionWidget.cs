/*
 * The base library for ProbeNet print exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using InternationalizationUi;
using Internationalization;
using ProbeNet.Export;
using ProbeNet.Messages;

namespace ProbeNet.Export.Print
{
    /// <summary>
    /// Widget for selecting the alignment.
    /// </summary>
    public class AlignmentSelectionWidget: Gtk.VBox
    {
        /// <summary>
        /// Delegate method for selection changed event.
        /// </summary>
        public delegate void SelectionChangedDelegate();
        /// <summary>
        /// Occurs when selection changed.
        /// </summary>
        public event SelectionChangedDelegate SelectionChanged;

        private IList<Gtk.CheckButton> checkButtonList;
        private IList<MeasurementAlignmentIdentifier> alignments;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.Print.AlignmentSelectionWidget"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="alignments">Alignments.</param>
        public AlignmentSelectionWidget(I18n i18n, IList<MeasurementAlignmentIdentifier> alignments):
            base(true, 5)
        {
            this.alignments = alignments;

            checkButtonList = new List<Gtk.CheckButton>();
            foreach (MeasurementAlignmentIdentifier identifier in alignments) {
                TranslationString label = identifier.GetLabel(
                    i18n,
                    i18n.TrObject(Constants.UiDomain, "Data"));
                Gtk.CheckButton checkButton = new TranslatableCheckButton(label) {
                    Active = true
                };
                checkButton.Toggled += HandleCheckButtonToggled;
                checkButtonList.Add(checkButton);
                PackStart(checkButton, false, false, 0);
            }
        }

        private void HandleCheckButtonToggled (object sender, EventArgs e)
        {
            UpdateSensitivity(ActiveAlignments);
            if (SelectionChanged != null) {
                SelectionChanged();
            }
        }

        private int ActiveAlignments {
            get {
                int count = 0;
                foreach (bool selected in SelectedAlignments.Values) {
                    if (selected) {
                        count++;
                    }
                }
                return count;
            }
        }

        private void UpdateSensitivity(int count)
        {
            Gtk.Application.Invoke(delegate {
                foreach (Gtk.CheckButton checkButton in checkButtonList) {
                    bool sensitive = !((count == 1) && checkButton.Active);
                    if (checkButton.Sensitive != sensitive) {
                        checkButton.Sensitive = sensitive;
                    }
                }
            });
        }

        /// <summary>
        /// Gets the selected alignments.
        /// </summary>
        /// <value>The selected alignments.</value>
        public IDictionary<MeasurementAlignmentIdentifier, bool> SelectedAlignments {
            get {
                IDictionary<MeasurementAlignmentIdentifier, bool> dictionary =
                    new Dictionary<MeasurementAlignmentIdentifier, bool>();
                for (int i = 0; i < this.alignments.Count; i++) {
                    dictionary.Add(alignments[i], checkButtonList[i].Active);
                }
                return dictionary;
            }
        }
    }
}

