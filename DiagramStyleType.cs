using System;
using System.ComponentModel;
using Internationalization;

namespace ProbeNet.Export.Print
{
    /// <summary>
    /// Diagram style type.
    /// </summary>
    public enum DiagramStyleType
    {
        /// <summary>
        /// The lines of the diagram are colored solid lines.
        /// </summary>
        [TranslatableCaption(Constants.UiDomain, "Colored with solid line")]
        [Description("coloredWithSolidLine")]
        ColoredWithSolidLine,

        /// <summary>
        /// The lines of the diagram are grayscale solid lines.
        /// </summary>
        [TranslatableCaption(Constants.UiDomain, "Grayscale with solid line")]
        [Description("grayscaleWithSolidLine")]
        GrayscaleWithSolidLine,

        /// <summary>
        /// The lines of the diagram are grayscale dashed lines.
        /// </summary>
        [TranslatableCaption(Constants.UiDomain, "Grayscale with dashed line")]
        [Description("grayscaleWithDashedLine")]
        GrayscaleWithDashedLines,

        /// <summary>
        /// THe lines of the diagram are black dashed lines.
        /// </summary>
        [TranslatableCaption(Constants.UiDomain, "Black with dashed line")]
        [Description("blackWithDashedLine")]
        BlackWithDashedLine
    }
}

