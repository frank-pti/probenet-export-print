using Frank.Widgets;
using Frank.Widgets.Model;
using FrankPti.Collections;
using Gtk;
using Internationalization;
using InternationalizationUi;
/*
 * The base library for ProbeNet print exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace ProbeNet.Export.Print
{
    /// <summary>
    /// Widget for setting general print settings.
    /// </summary>
    public class GeneralPrintSettingsWidget : VBox
    {
        /// <summary>
        /// Delegate method for changed events.
        /// </summary>
        public delegate void ChangedDelegate();
        /// <summary>
        /// Occurs when print ranges changed.
        /// </summary>
        public event ChangedDelegate PrintRangesChanged;
        /// <summary>
        /// Occurs when measurement page break changed.
        /// </summary>
        public event ChangedDelegate MeasurementPageBreakChanged;
        /// <summary>
        /// Occurs when sample alignments changed.
        /// </summary>
        public event ChangedDelegate SampleAlignmentsChanged;
        /// <summary>
        /// Occurs when paper size changed.
        /// </summary>
        public event ChangedDelegate PaperSizeChanged;
        /// <summary>
        /// Occurs when paper orientation changed.
        /// </summary>
        public event ChangedDelegate PaperOrientationChanged;
        /// <summary>
        /// Occurs when chart scale changed.
        /// </summary>
        public event ChangedDelegate ChartScaleChanged;
        /// <summary>
        /// Occurs when diagram style changed.
        /// </summary>
        public event ChangedDelegate DiagramStyleChanged;

        //private TreeView printerListView;
        private ComboBox paperSizeComboBox;
        private TranslatableEnumCheckButtonVBox<PrintRange> printRangeVBox;
        private TranslatableEnumCheckButtonVBox<PageBreakType> pageBreakTypesVBox;
        private SpinButton chartScaleSpinButton;
        private RadioButton orientationPortraitButton;
        private RadioButton orientationLandscapeButton;
        private AlignmentSelectionWidget alignmentSelectionWidget;
        private TranslatableEnumCombobox<DiagramStyleType> diagramStyleComboBox;

        private ListStore paperSizesList;
        private Dictionary<string, PaperSize> paperSizes;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.Print.GeneralPrintSettingsWidget"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="printers">Printers.</param>
        /// <param name="measurementAlignments">Measurement alignments.</param>
        /// <param name="printRanges">Print ranges.</param>
        /// <param name="pageBreakTypes">Page break types.</param>
        /// <param name="diagramStyle">Diagram style.</param>
        /// <param name="chartAvailable">If set to <c>true</c> chart available.</param>
        /// <param name="attributesAvailable">If set to <c>true</c> attributes available.</param>
        /// <param name="parametersAvailable">If set to <c>true</c> parameters available.</param>
        /// <param name="measurementCount">Measurement count.</param>
        public GeneralPrintSettingsWidget(
            I18n i18n,
            IList<PrinterInfo> printers,
            IList<MeasurementAlignmentIdentifier> measurementAlignments,
            EnumDictionary<PrintRange, bool> printRanges,
            EnumDictionary<PageBreakType, bool> pageBreakTypes,
            DiagramStyleType diagramStyle,
            bool chartAvailable, bool attributesAvailable, bool parametersAvailable, int measurementCount)
        {
            // Printer
            TranslatableLabel printerSelectionHintLabel = new TranslatableLabel(
                i18n, Constants.UiDomain, "The printer can be chosen after clicking 'Print'.") { Xalign = 0f };
            Alignment printerSelectionHintAlignment = new FrameAlignment(5);
            printerSelectionHintAlignment.Add(printerSelectionHintLabel);
            Frame printerFrame = new TranslatableFrame(i18n, Constants.UiDomain, "Printer");
            printerFrame.Add(printerSelectionHintAlignment);

            // Paper size
            paperSizes = new Dictionary<string, PaperSize>();
            paperSizesList = new ListStore(typeof(string), typeof(string));
            foreach (Gtk.PaperSize paperSize in PaperSize.GetPaperSizes(false)) {
                if (!paperSizes.ContainsKey(paperSize.Name)) {
                    paperSizesList.AppendValues(paperSize.DisplayName, paperSize.Name);
                    paperSizes.Add(paperSize.Name, paperSize);
                }
            }
            paperSizeComboBox = new ComboBox(paperSizesList);
            CellRendererText paperSizeNameRenderer = new CellRendererText();
            paperSizeComboBox.PackStart(paperSizeNameRenderer, false);
            paperSizeComboBox.AddAttribute(paperSizeNameRenderer, "text", 0);
            AcitvatePaperSize(PaperSize.Default);

            VBox paperPropertiesBox = new VBox(true, 0);
            paperPropertiesBox.PackStart(paperSizeComboBox, false, false, 3);
            Alignment paperPropertiesAlignment = new FrameAlignment(5);
            paperPropertiesAlignment.Add(paperPropertiesBox);
            Frame paperPropertiesFrame = new TranslatableFrame(i18n, Constants.UiDomain, "Paper Properties");
            paperPropertiesFrame.Add(paperPropertiesAlignment);
            paperSizeComboBox.Changed += (o, e) => HandleChanged(PaperSizeChanged);

            // Paper orientation
            Table orientationTable = new Table(2, 1, false);
            orientationPortraitButton = new TranslatableRadioButton(null, i18n, Constants.UiDomain, "Portrait");
            HBox orientationPortraitBox = BuildOrientationBox(orientationPortraitButton,
                Image.NewFromIconName(Stock.OrientationPortrait, IconSize.Button));
            orientationLandscapeButton = new TranslatableRadioButton(orientationPortraitButton, i18n, Constants.UiDomain, "Landscape");
            HBox orientationLandscapeBox = BuildOrientationBox(orientationLandscapeButton,
                Image.NewFromIconName(Stock.OrientationLandscape, IconSize.Button));
            orientationTable.Attach(orientationPortraitBox, 0, 1, 0, 1);
            orientationTable.Attach(orientationLandscapeBox, 1, 2, 0, 1);
            paperPropertiesBox.PackStart(orientationTable, false, false, 2);
            orientationPortraitButton.Toggled += (o, e) => HandleChanged(PaperOrientationChanged);

            // Page breaks
            pageBreakTypes[PageBreakType.AfterEachMeasurement] =
                pageBreakTypes[PageBreakType.AfterEachMeasurement] && measurementCount > 1;
            pageBreakTypes[PageBreakType.ScaleOnlyToKeepChartOnPage] =
                pageBreakTypes[PageBreakType.ScaleOnlyToKeepChartOnPage] && chartAvailable;
            pageBreakTypesVBox = new TranslatableEnumCheckButtonVBox<PageBreakType>(i18n) {
                Active = pageBreakTypes
            };
            IDictionary<PageBreakType, bool> sensitiveness = pageBreakTypesVBox.SensitiveEntries;
            sensitiveness[PageBreakType.AfterEachMeasurement] = measurementCount > 1;
            sensitiveness[PageBreakType.ScaleOnlyToKeepChartOnPage] = chartAvailable;
            pageBreakTypesVBox.SensitiveEntries = sensitiveness;
            pageBreakTypesVBox.Toggled += (entry, value) => HandleChanged(MeasurementPageBreakChanged);
            Label chartScaleFactorLabel = new TranslatableLabel(i18n, Constants.UiDomain, "Chart scale factor") {
                Xalign = 0f,
                Sensitive = chartAvailable
            };
            chartScaleSpinButton = new SpinButton(new Adjustment(1, 0, 1, 0.01, 0.1, 0), 0.01, 2) {
                Sensitive = chartAvailable
            };
            chartScaleSpinButton.Changed += (sender, e) => HandleChanged(ChartScaleChanged);
            Table pageBreaksTable = new Table(2, 2, false);
            pageBreaksTable.Attach(pageBreakTypesVBox, 0, 2, 0, 1);
            pageBreaksTable.Attach(chartScaleFactorLabel, 0, 1, 1, 2);
            pageBreaksTable.Attach(chartScaleSpinButton, 1, 2, 1, 2);
            Alignment pageBreaksFrameAlignment = new FrameAlignment(5);
            pageBreaksFrameAlignment.Add(pageBreaksTable);
            Frame pageBreaksFrame = new TranslatableFrame(i18n, Constants.UiDomain, "Page breaks");
            pageBreaksFrame.Add(pageBreaksFrameAlignment);

            // Alignment
            Frame alignmentFrame = null;
            if (measurementAlignments != null && measurementAlignments.Count > 0) {
                alignmentSelectionWidget = new AlignmentSelectionWidget(i18n, measurementAlignments) {
                    Sensitive = (measurementAlignments.Count > 1)
                };
                alignmentSelectionWidget.SelectionChanged += () => HandleChanged(SampleAlignmentsChanged);
                Alignment alignmentFrameAlignment = new FrameAlignment(5);
                alignmentFrameAlignment.Add(alignmentSelectionWidget);
                alignmentFrame = new TranslatableFrame(i18n, Constants.UiDomain, "Sample alignments");
                alignmentFrame.Add(alignmentFrameAlignment);
            }

            // Print Range
            printRanges[PrintRange.Attributes] = printRanges[PrintRange.Attributes] && attributesAvailable;
            printRanges[PrintRange.Parameters] = printRanges[PrintRange.Parameters] && parametersAvailable;
            printRanges[PrintRange.Chart] = printRanges[PrintRange.Chart] && chartAvailable;
            printRangeVBox = new TranslatableEnumCheckButtonVBox<PrintRange>(i18n) {
                Active = printRanges,
            };
            IDictionary<PrintRange, bool> sensitiveEntries = printRangeVBox.SensitiveEntries;
            sensitiveEntries[PrintRange.Attributes] = attributesAvailable;
            sensitiveEntries[PrintRange.Parameters] = parametersAvailable;
            sensitiveEntries[PrintRange.Chart] = chartAvailable;
            printRangeVBox.SensitiveEntries = sensitiveEntries;
            printRangeVBox.Toggled += (o, e) => HandleChanged(PrintRangesChanged);

            Alignment rangeFrameAlignment = new FrameAlignment(5);
            rangeFrameAlignment.Add(printRangeVBox);

            Frame rangeFrame = new TranslatableFrame(i18n, Constants.UiDomain, "Range");
            rangeFrame.Add(rangeFrameAlignment);

            // Diagram style
            diagramStyleComboBox = new TranslatableEnumCombobox<DiagramStyleType>(i18n, Constants.UiDomain) {
                ActiveEnum = diagramStyle,
                Sensitive = printRangeVBox.SensitiveEntries[PrintRange.Chart] && printRangeVBox.Active[PrintRange.Chart]
            };
            diagramStyleComboBox.Changed += (o, e) => HandleChanged(DiagramStyleChanged);
            Table stylesTable = new Table(1, 2, false);
            stylesTable.Attach(new TranslatableLabel(i18n, Constants.UiDomain, "Diagram") {
                Xalign = 0f
            }, 0, 1, 0, 1);
            stylesTable.Attach(diagramStyleComboBox, 1, 2, 0, 1);
            Alignment stylesAlignment = new FrameAlignment(5);
            stylesAlignment.Add(stylesTable);
            Frame stylesFrame = new TranslatableFrame(i18n, Constants.UiDomain, "Styles");
            stylesFrame.Add(stylesAlignment);

            // pack together
            VBox overallBox = new VBox(false, 3);
            overallBox.PackStart(printerFrame, false, true, 3);
            overallBox.PackStart(paperPropertiesFrame, false, false, 3);
            if (pageBreaksFrame != null) {
                overallBox.PackStart(pageBreaksFrame, false, false, 3);
            }
            if (alignmentFrame != null) {
                overallBox.PackStart(alignmentFrame, false, false, 3);
            }
            overallBox.PackStart(rangeFrame, false, false, 3);
            overallBox.PackStart(stylesFrame, false, false, 3);

            Alignment overallAlignment = new FrameAlignment(5);
            overallAlignment.Add(overallBox);

            PackStart(overallAlignment);
        }

        private static HBox BuildOrientationBox(RadioButton radioButton, Image image)
        {
            HBox box = new HBox(false, 2);
            box.PackStart(radioButton, false, true, 0);
            box.PackStart(image, false, false, 0);
            return box;
        }

        private void HandleChanged(ChangedDelegate changedDelegate)
        {
            diagramStyleComboBox.Sensitive =
                printRangeVBox.SensitiveEntries[PrintRange.Chart] && printRangeVBox.Active[PrintRange.Chart];
            if (changedDelegate != null) {
                changedDelegate();
            }
        }

        private void RenderPrinterName(TreeViewColumn column, CellRenderer cell, TreeModel model, TreeIter iter)
        {
            if (cell is CellRendererText) {
                PrinterInfo printer = (PrinterInfo)model.GetValue(iter, 0);
                (cell as CellRendererText).Text = printer.Name;
            }
        }

        private void AcitvatePaperSize(string paperSize)
        {
            bool found = false;
            TreeIter iter;
            bool next = paperSizesList.GetIterFirst(out iter);
            while (next && !found && paperSizesList.IterIsValid(iter)) {
                string val = (string)paperSizesList.GetValue(iter, 1);
                if (val == paperSize) {
                    paperSizeComboBox.SetActiveIter(iter);
                    found = true;
                }
                next = paperSizesList.IterNext(ref iter);
            }
        }

        /// <summary>
        /// Gets the print ranges.
        /// </summary>
        /// <value>The print ranges.</value>
        public EnumDictionary<PrintRange, bool> PrintRanges
        {
            get
            {
                return printRangeVBox.Active;
            }
        }

        /// <summary>
        /// Gets the size of the selected paper.
        /// </summary>
        /// <value>The size of the selected paper.</value>
        public PaperSize SelectedPaperSize
        {
            get
            {
                TreeIter iter;
                if (paperSizeComboBox.GetActiveIter(out iter)) {
                    string size = (string)paperSizeComboBox.Model.GetValue(iter, 1);
                    return paperSizes[size];
                }
                throw new InvalidOperationException("No paper size is selected.");
            }
        }

        /// <summary>
        /// Gets or sets the paper orientation.
        /// </summary>
        /// <value>The paper orientation.</value>
        public PageOrientation PaperOrientation
        {
            get
            {
                if (orientationLandscapeButton.Active) {
                    return PageOrientation.Landscape;
                }
                return PageOrientation.Portrait;
            }
            set
            {
                if (value == PageOrientation.Landscape || value == PageOrientation.ReverseLandscape) {
                    orientationLandscapeButton.Active = true;
                } else {
                    orientationPortraitButton.Active = true;
                }
            }
        }

        /// <summary>
        /// Gets the paper.
        /// </summary>
        /// <value>The paper.</value>
        public PaperInformation Paper
        {
            get
            {
                PaperInformation paper = new PaperInformation(SelectedPaperSize);
                if (PaperOrientation == PageOrientation.Landscape) {
                    paper.RotateLeft();
                }
                return paper;
            }
        }

        /// <summary>
        /// Gets the selected alignment identifiers.
        /// </summary>
        /// <value>The selected alignment identifiers.</value>
        public IDictionary<MeasurementAlignmentIdentifier, bool> SelectedAlignmentIdentifiers
        {
            get
            {
                if (alignmentSelectionWidget == null) {
                    return null;
                }
                return alignmentSelectionWidget.SelectedAlignments;
            }
        }

        /// <summary>
        /// Gets the page break types.
        /// </summary>
        /// <value>The page break types.</value>
        public EnumDictionary<PageBreakType, bool> PageBreakTypes
        {
            get
            {
                return pageBreakTypesVBox.Active;
            }
        }

        /// <summary>
        /// Gets the chart scale.
        /// </summary>
        /// <value>The chart scale.</value>
        public double ChartScale
        {
            get
            {
                return chartScaleSpinButton.Value;
            }
        }

        /// <summary>
        /// Gets the diagram style.
        /// </summary>
        /// <value>The diagram style.</value>
        public DiagramStyleType DiagramStyle
        {
            get
            {
                return diagramStyleComboBox.ActiveEnum;
            }
        }
    }
}

