/*
 * The base library for ProbeNet print exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace ProbeNet.Export.Print
{
    /// <summary>
    /// Helper class.
    /// </summary>
    public static class Helper
    {
        /// <summary>
        /// Builds the name of the resource.
        /// </summary>
        /// <returns>The resource name.</returns>
        /// <param name="prefix">Prefix.</param>
        /// <param name="name">Name.</param>
        public static string BuildResourceName(string prefix, string name)
        {
            return String.Format("{0}.{1}", prefix, name);
        }

        /// <summary>
        /// Loads the gtk image from resource.
        /// </summary>
        /// <returns>The gtk image from resource.</returns>
        /// <param name="prefix">Prefix.</param>
        /// <param name="name">Name.</param>
        public static Gtk.Image LoadGtkImageFromResource(string prefix, string name)
        {
            return Gtk.Image.LoadFromResource(BuildResourceName(prefix, name));
        }

        /// <summary>
        /// Convert gdk color to cairo color.
        /// </summary>
        /// <returns>The cairo color.</returns>
        /// <param name="original">The gdk color.</param>
        public static Cairo.Color GdkToCairoColor(Gdk.Color original)
        {
            return new Cairo.Color(
                original.Red * 1.0 / 65535.0,
                original.Green * 1.0 / 65535.0,
                original.Blue * 1.0 / 65535.0);
        }
    }
}

