/*
 * The base library for ProbeNet print exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.ComponentModel;
using Internationalization;

namespace ProbeNet.Export.Print
{
    /// <summary>
    /// Page break type.
    /// </summary>
    public enum PageBreakType
    {
        /// <summary>
        /// Insert page break before first measurement.
        /// </summary>
        [TranslatableCaption(Constants.UiDomain, "Before first measurement")]
        [Description("beforeFirstMeasurement")]
        BeforeFirstMeasurement,

        /// <summary>
        /// Insert page break after each measurement.
        /// </summary>
        [TranslatableCaption(Constants.UiDomain, "After each measurement")]
        [Description("afterEachMeasurement")]
        AfterEachMeasurement,

        /// <summary>
        /// Scale chart only if needed to keep chart on the same page with the data.
        /// </summary>
        [TranslatableCaption(Constants.UiDomain, "Scale only to keep chart on page")]
        [Description("scaleOnlyToKeepChartOnPage")]
        ScaleOnlyToKeepChartOnPage
    }
}

