/*
 * The base library for ProbeNet print exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Gtk;
using Internationalization;
using System.Globalization;

namespace ProbeNet.Export.Print
{
    /// <summary>
    /// Widget for controlling the page preview.
    /// </summary>
	public class PagePreviewControlWidget: HBox
	{
		private Button previousPageButton;
		private Button nextPageButton;
		private Entry currentPageEntry;
		private Label totalPagesLabel;
		private I18n i18n;

        /// <summary>
        /// Delegate method for the go to page event.
        /// </summary>
		public delegate void GoToPageDelegate(int page);
        /// <summary>
        /// Occurs when go to page.
        /// </summary>
		public event GoToPageDelegate GoToPage;

		public PagePreviewControlWidget (I18n i18n, int currentPage, int totalPages):
			base(false, 0)
		{
			this.i18n = i18n;

			previousPageButton = new Button() {
				UseStock = true,
				Image = Helper.LoadGtkImageFromResource(Constants.IconResourcePrefix, "go-previous.png")
			};
			previousPageButton.Clicked += delegate {
				GoBack();
			};
			nextPageButton = new Button() {
				UseStock = true,
				Image = Helper.LoadGtkImageFromResource(Constants.IconResourcePrefix, "go-next.png")
			};
			nextPageButton.Clicked += delegate {
				GoForward();
			};
			currentPageEntry = new Entry(String.Format(i18n.NumberFormat, "{0}", currentPage)) {
				Xalign = 1f
			};
			currentPageEntry.Changed += HandleCurrentPageEntryChanged;
			totalPagesLabel = new Label(String.Format(i18n.NumberFormat, "{0}", totalPages));
			Label slashLabel = new Label("/");

			PackStart(previousPageButton, false, true, 3);
			PackStart(currentPageEntry, true, true, 3);
			PackStart(slashLabel, false, true, 3);
			PackStart(totalPagesLabel, false, true, 3);
			PackStart(nextPageButton, false, true, 3);

			UpdateSensitivities(currentPage, totalPages);
		}

		private void GoBack() {
			int page = ParseCurrentPageNumber();
			if (page > 1) {
				page--;
			}
			NotifyPageNumber(page);
			int totalPages = ParseTotalPagesNumber();
			UpdateSensitivities(page, totalPages);
		}

		private void GoForward() {
			int page = ParseCurrentPageNumber();
			int totalPages = ParseTotalPagesNumber();
			if (page < totalPages) {
				page++;
			}
			NotifyPageNumber(page);
			UpdateSensitivities(page, totalPages);
		}

		private void HandleCurrentPageEntryChanged (object sender, EventArgs e)
		{
			int currentPage = ParseCurrentPageNumber();
			NotifyPageNumber(currentPage);
		}

		private int ParseCurrentPageNumber()
		{
			int val;
			bool success = int.TryParse(currentPageEntry.Text, NumberStyles.Any, i18n.NumberFormat, out val);
			if (success) {
				return val;
			}
			return -1;
		}

		private int ParseTotalPagesNumber()
		{
			int val;
			int.TryParse(totalPagesLabel.Text, NumberStyles.Any, i18n.NumberFormat, out val);
			return val;
		}

		private void NotifyPageNumber(int pageNumber)
		{
			int totalPages = ParseTotalPagesNumber();
			if (GoToPage != null && pageNumber >= 0 && pageNumber <= totalPages) {
				string pageNumberString = String.Format(i18n.NumberFormat, "{0}", pageNumber);
				if (currentPageEntry.Text != pageNumberString) {
					currentPageEntry.Changed -= HandleCurrentPageEntryChanged;
					currentPageEntry.Text = pageNumberString;
					currentPageEntry.Changed += HandleCurrentPageEntryChanged;
				}
				GoToPage(pageNumber);
				UpdateSensitivities(pageNumber, totalPages);
			}
		}

		private void UpdateSensitivities(int page, int totalPages)
		{
			previousPageButton.Sensitive = (page > 1);
			nextPageButton.Sensitive = (page < totalPages);
		}

        /// <summary>
        /// Gets or sets the page count.
        /// </summary>
        /// <value>The page count.</value>
		public int PageCount {
			get {
				return ParseTotalPagesNumber();
			}
			set {
				int currentPage = ParseCurrentPageNumber();
				totalPagesLabel.Text = String.Format(i18n.NumberFormat, "{0}", value);
				if (currentPage > value) {
                    currentPageEntry.Text = String.Format(i18n.NumberFormat, "{0}", value);
				}
				UpdateSensitivities(currentPage, value);
			}
		}
	}
}

