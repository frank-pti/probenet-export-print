/*
 * The base library for ProbeNet print exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace ProbeNet.Export.Print
{
    /// <summary>
    /// Page preview widget.
    /// </summary>
    public class PagePreviewWidget: Gtk.DrawingArea
    {
        public delegate void PageCountChangedDelegate();
        public event PageCountChangedDelegate PageCountChanged;

        private const double PREVIEW_SCALE = 1.5;

        private const int PAPER_LINE_WIDTH = 1;
        private const int MIN_SIZE = 100;

        private const int MIN_PADDING_X = 10;
        private const int MIN_PADDING_Y = 10;
        private const int DROP_SHADOW_OFFSET_X = 5;
        private const int DROP_SHADOW_OFFSET_Y = 5;

        private Cairo.Rectangle virtualPaper;
        private double allocatedWidth;
        private double allocatedHeight;

        private ReportPainter painter;

        private int page;
        private int pageCount;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.Print.PagePreviewWidget"/> class.
        /// </summary>
        public PagePreviewWidget():
            this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.Print.PagePreviewWidget"/> class.
        /// </summary>
        /// <param name="painter">Painter.</param>
        public PagePreviewWidget (ReportPainter painter)
        {
            this.painter = painter;
        }

        /// <summary>
        /// Gets or sets the report painter.
        /// </summary>
        /// <value>The painter.</value>
        public ReportPainter Painter {
            get {
                return painter;
            }
            set {
                if (painter != value) {
                    painter = value;
                    CurrentPage = 0;
                    QueueDraw();
                }
            }
        }

        /// <summary>
        /// Gets or sets the current page.
        /// </summary>
        /// <value>The current page.</value>
        public int CurrentPage
        {
            get {
                return page;
            }
            set {
                if (page != value) {
                    page = value;
                    QueueDraw();
                }
            }
        }

        /// <summary>
        /// Gets the page count.
        /// </summary>
        /// <value>The page count.</value>
        public int PageCount
        {
            get {
                return pageCount;
            }
            private set {
                if (pageCount != value) {
                    pageCount = value;
                    if (PageCountChanged != null) {
                        PageCountChanged();
                    }
                }
            }
        }

        private double RequiredWidth {
            get {
                return MIN_SIZE + MinSpacingX;
            }
        }

        private double RequiredHeight {
            get {
                return MIN_SIZE + MinSpacingY;
            }
        }

        private int MinSpacingX {
            get {
                return 2 * MIN_PADDING_X + DROP_SHADOW_OFFSET_X;
            }
        }

        private int MinSpacingY {
            get {
                return 2 * MIN_PADDING_Y + DROP_SHADOW_OFFSET_Y;
            }
        }

        /// <inheritdoc/>
        protected override bool OnExposeEvent (Gdk.EventExpose evnt)
        {
            base.OnExposeEvent (evnt);

            if (painter == null) {
                return true;
            }

            if (allocatedWidth/allocatedHeight < painter.Paper.Width/painter.Paper.Height) {
                double virtualPaperHeight = allocatedWidth - MinSpacingX;
                double virtualPaperWidth = painter.Paper.Height * virtualPaperHeight / painter.Paper.Width;
                virtualPaper = new Cairo.Rectangle(
                    MIN_PADDING_X, allocatedHeight / 2 - virtualPaperWidth / 2,
                    virtualPaperHeight, virtualPaperWidth
                );
            } else {
                double virtualPaperHeight = allocatedHeight - MinSpacingY;
                double virtualPaperWidth = painter.Paper.Width * (allocatedHeight - MinSpacingY) / painter.Paper.Height;
                virtualPaper = new Cairo.Rectangle(
                    allocatedWidth / 2 - virtualPaperWidth/2, MIN_PADDING_Y,
                    virtualPaperWidth, virtualPaperHeight
                );
            }

            Cairo.Context context = Gdk.CairoHelper.Create(evnt.Window);
            
            painter.Context = context;
            PageCount = painter.CalculatePages(context);

            context.Color = new Cairo.Color(1, 1, 1);
            context.Paint();

            context.Save();
            {
                PaintPaper (context);
            }
            context.Restore();

            context.Save();
            {
                context.Translate(virtualPaper.X, virtualPaper.Y);
                double scale = virtualPaper.Width / painter.Paper.Width;
                context.Scale(scale, scale);
                painter.Paint(context, page);
            }
            context.Restore();

            (context as IDisposable).Dispose();

            return true;
        }

        private void PaintPaper (Cairo.Context context)
        {
            context.Save();
            {
                PaintPaperDropShadow (context);
            }
            context.Restore();
            
            context.LineWidth = PAPER_LINE_WIDTH;
            context.Rectangle(virtualPaper);
            context.Color = new Cairo.Color(1, 1, 1);
            context.FillPreserve();
            context.Color = new Cairo.Color(0, 0, 0);
            context.Stroke();
        }

        private void PaintPaperDropShadow (Cairo.Context context)
        {
            context.Translate(DROP_SHADOW_OFFSET_X, DROP_SHADOW_OFFSET_Y);
            context.Color = new Cairo.Color(0.7, 0.7, 0.7);
            context.Rectangle(virtualPaper);
            context.Fill();
        }

        /// <inheritdoc/>
        protected override void OnSizeRequested (ref Gtk.Requisition requisition)
        {
            requisition.Width = (int)RequiredWidth;
            requisition.Height = (int)RequiredHeight;
        }

        /// <inheritdoc/>
        protected override void OnSizeAllocated (Gdk.Rectangle allocation)
        {
            allocatedWidth = allocation.Width;
            allocatedHeight = allocation.Height;

            base.OnSizeAllocated (allocation);
        }
    }
}
