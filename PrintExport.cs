using Frank.Widgets.Model;
using Internationalization;
using Logging;
/*
 * The base library for ProbeNet print exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace ProbeNet.Export.Print
{
    /// <summary>
    /// Base class for print exports.
    /// </summary>
    public abstract class PrintExport : DataExport
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.Print.PrintExport"/> class.
        /// </summary>
        /// <param name="settingsProvider">Settings provider.</param>
        protected PrintExport(SettingsProvider settingsProvider) :
            base(settingsProvider)
        {
        }

        /// <summary>
        /// Gets the dialog.
        /// </summary>
        /// <returns>The dialog.</returns>
        /// <param name="i18n">I18n.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="exportList">Export list.</param>
        /// <param name="preselectExport">Preselect export.</param>
        /// <param name="measurements">Measurements.</param>
        /// <param name="parameter">Parameter.</param>
        /// <param name="settingsProvider">Settings provider.</param>
        public static PrintPreviewDialog GetDialog(
            I18n i18n,
            Gtk.Window parent,
            IList<IExportInformation> exportList,
            IExportInformation preselectExport,
            IList<MeasurementSetAdapter> measurements,
            PrintParameter parameter,
            SettingsProvider settingsProvider)
        {
            return new PrintPreviewDialog(
                i18n, parent, exportList, preselectExport, measurements, parameter, settingsProvider);
        }

        /// <summary>
        /// Gets or sets the report painter.
        /// </summary>
        /// <value>The report painter.</value>
        public abstract ReportPainter ReportPainter
        {
            get;
            protected set;
        }

        /// <summary>
        /// Builds the report painter.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="paper">Paper.</param>
        /// <param name="measurements">Measurements.</param>
        /// <param name="parameter">Parameter.</param>
        /// <param name="selectedAlignments">Selected alignments.</param>
        public abstract void BuildReportPainter(
            I18n i18n, PaperInformation paper,
            IList<MeasurementSetAdapter> measurements,
            PrintParameter parameter,
            IDictionary<MeasurementAlignmentIdentifier, bool> selectedAlignments);

        /// <summary>
        /// Export the specified i18n, printer, paperSize and pageOrientation.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="printer">Printer.</param>
        /// <param name="paperSize">Paper size.</param>
        /// <param name="pageOrientation">Page orientation.</param>
        public void Export(I18n i18n, Gtk.Window parent, Gtk.PaperSize paperSize, Gtk.PageOrientation pageOrientation)
        {
            if (ReportPainter == null) {
                throw new NullReferenceException("No report painter set.");
            }

            Gtk.PrintOperation printOperation = new Gtk.PrintOperation();
            printOperation.PrintSettings = new Gtk.PrintSettings();
            printOperation.BeginPrint += HandlePrintOperationBeginPrint;
            printOperation.DrawPage += HandlePrintOperationDrawPage;
            printOperation.DefaultPageSetup = new Gtk.PageSetup();
            printOperation.DefaultPageSetup.PaperSize = paperSize;
            printOperation.DefaultPageSetup.Orientation = pageOrientation;

            try {
                printOperation.Run(Gtk.PrintOperationAction.PrintDialog, parent);   // for choosing printer
                printOperation.Run(Gtk.PrintOperationAction.Print, parent);
            } catch (Exception e) {
                throw new InvalidOperationException(
                    "Print operation failed! Print job canceled or printer disconnected.", e);
            }
        }

        private void HandlePrintOperationBeginPrint(object o, Gtk.BeginPrintArgs args)
        {
            try {
                Gtk.PrintOperation printOperation = o as Gtk.PrintOperation;
                printOperation.NPages = ReportPainter.CalculatePages(args.Context.CairoContext);
            } catch (Exception e) {
                Logger.Log(e);
            }
        }

        private void HandlePrintOperationDrawPage(object o, Gtk.DrawPageArgs args)
        {
            try {
                double width = args.Context.PageSetup.GetPageWidth(Gtk.Unit.Points);

                Cairo.Context context = args.Context.CairoContext;
                double scale = width / ReportPainter.Paper.Width;

                if (Frank.Widgets.Helper.WindowsDetected) {
                    scale *= Constants.WindowsPrintFactor;
                    Logger.Log("Windows detected, manipulating print scale factor to {0}", scale);
                }

                context.Scale(scale, scale);

                ReportPainter.Context = context;
                ReportPainter.Paint(context, args.PageNr);
                (context as IDisposable).Dispose();
            } catch (Exception e) {
                Logger.Log(e);
            }
        }
    }
}

