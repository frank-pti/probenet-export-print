using Internationalization;
using InternationalizationUi;
/*
 * The base library for ProbeNet print exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace ProbeNet.Export.Print
{
    /// <summary>
    /// Widget for Print Layout
    /// </summary>
    public class PrintLayoutWidget : Gtk.HBox
    {
        /// <summary>
        /// Occurs when data export changed.
        /// </summary>
        public event EventHandler DataExportChanged;

        private DataExportComboBox exportChooser;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.Print.PrintLayoutWidget"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="exportList">Export list.</param>
        public PrintLayoutWidget(I18n i18n, IList<IExportInformation> exportList) :
            base(false, 8)
        {
            exportChooser = new DataExportComboBox(i18n, exportList);
            exportChooser.Changed += delegate(object sender, EventArgs e) {
                if (DataExportChanged != null) {
                    DataExportChanged(sender, e);
                }
            };
            PackStart(new TranslatableLabel(i18n, Constants.UiDomain, "Layout:"), false, false, 8);
            PackStart(exportChooser, false, false, 8);
        }

        /// <summary>
        /// Gets or sets the selected layout.
        /// </summary>
        /// <value>The selected layout.</value>
        public IExportInformation SelectedLayout
        {
            get
            {
                return exportChooser.ActiveItem;
            }
            set
            {
                exportChooser.ActiveItem = value;
            }
        }
    }
}

