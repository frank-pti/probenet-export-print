/*
 * The base library for ProbeNet print exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using CairoChart.Drawing;
using FrankPti.Collections;

namespace ProbeNet.Export.Print
{
    /// <summary>
    /// Print parameter.
    /// </summary>
    public class PrintParameter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.Print.PrintParameter"/> class.
        /// </summary>
        /// <param name="resultOrientation">Result orientation.</param>
        /// <param name="resultNumberingFormat">Result numbering format.</param>
        /// <param name="companyName">Company name.</param>
        /// <param name="companyPlace">Company place.</param>
        /// <param name="companySign">Company sign.</param>
        /// <param name="companySignNextToText">If set to <c>true</c> company sign next to text.</param>
        /// <param name="printRanges">Print ranges.</param>
        /// <param name="nothingSubstitute">Nothing substitute.</param>
        /// <param name="nanSubstitute">Nan substitute.</param>
        /// <param name="numberFormat">Number format.</param>
        /// <param name="printDateTime">Print date time.</param>
        /// <param name="resultColumnSpacing">Result column spacing.</param>
        /// <param name="printNumberOfMeasurements">If set to <c>true</c> print number of measurements.</param>
        /// <param name="standardDeviationDecimalPlaces">Standard deviation decimal places.</param>
        /// <param name="statisticsFormats">Statistics formats.</param>
        /// <param name="pageBreakTypes">Page break types.</param>
        /// <param name="diagramStyle">Diagram style.</param>
        public PrintParameter (
            Orientation resultOrientation,
            ResultNumberingFormat resultNumberingFormat,
            string companyName,
            string companyPlace,
            string companySign,
            bool companySignNextToText,
            EnumDictionary<PrintRange, bool> printRanges,
            string nothingSubstitute,
            string nanSubstitute,
            string numberFormat,
            DateTime printDateTime,
            int resultColumnSpacing,
            bool printNumberOfMeasurements,
            Nullable<int> standardDeviationDecimalPlaces,
            IDictionary<string, NumericValueDisplayFormat> statisticsFormats,
            EnumDictionary<PageBreakType, bool> pageBreakTypes,
            DiagramStyleType diagramStyle)
        {
            ResultOrientation = resultOrientation;
            ResultNumberingFormat = resultNumberingFormat;
            CompanyName = companyName;
            CompanyPlace = companyPlace;
            CompanySign = companySign;
            CompanySignNextToText = companySignNextToText;
            PrintRanges = printRanges;
            NothingSubstitute = nothingSubstitute;
            NanSubstitute = nanSubstitute;
            NumberFormat = numberFormat;
            PrintDateTime = printDateTime;
            ResultColumnSpacing = resultColumnSpacing;
            PrintNumberOfMeasurements = printNumberOfMeasurements;
            StandardDeviationDecimalPlaces = standardDeviationDecimalPlaces;
            StatisticsFormats = statisticsFormats;
            PageBreakTypes = pageBreakTypes;
            DiagramStyle = diagramStyle;
        }

        /// <summary>
        /// Gets the result orientation.
        /// </summary>
        /// <value>The result orientation.</value>
        public Orientation ResultOrientation {
            get;
            private set;
        }

        /// <summary>
        /// Gets the result numbering format.
        /// </summary>
        /// <value>The result numbering format.</value>
        public ResultNumberingFormat ResultNumberingFormat {
            get;
            private set;
        }

        /// <summary>
        /// Gets the name of the company.
        /// </summary>
        /// <value>The name of the company.</value>
        public string CompanyName {
            get;
            private set;
        }

        /// <summary>
        /// Gets the company place.
        /// </summary>
        /// <value>The company place.</value>
        public string CompanyPlace {
            get;
            private set;
        }

        /// <summary>
        /// Gets the company sign.
        /// </summary>
        /// <value>The company sign.</value>
        public string CompanySign {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="ProbeNet.Export.Print.PrintParameter"/> company sign next to text.
        /// </summary>
        /// <value><c>true</c> if company sign next to text; otherwise, <c>false</c>.</value>
        public bool CompanySignNextToText {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the print ranges.
        /// </summary>
        /// <value>The print ranges.</value>
        public EnumDictionary<PrintRange, bool> PrintRanges {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the nothing substitute.
        /// </summary>
        /// <value>The nothing substitute.</value>
        public string NothingSubstitute {
            get;
            set;
        }

        /// <summary>
        /// Gets the substitute string for values that are not a number.
        /// </summary>
        /// <value>The substitute string for NaN values.</value>
        public string NanSubstitute {
            get;
            private set;
        }

        /// <summary>
        /// Gets the number format used to format values.
        /// </summary>
        /// <value>The number format.</value>
        public string NumberFormat {
            get;
            private set;
        }

        /// <summary>
        /// Gets the print date time.
        /// </summary>
        /// <value>The print date time.</value>
        public DateTime PrintDateTime {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="ProbeNet.Export.Print.PrintParameter"/> print number of measurements.
        /// </summary>
        /// <value><c>true</c> if print number of measurements; otherwise, <c>false</c>.</value>
        public bool PrintNumberOfMeasurements {
            get;
            private set;
        }

        /// <summary>
        /// Gets the standard deviation decimal places.
        /// </summary>
        /// <value>The standard deviation decimal places.</value>
        public Nullable<int> StandardDeviationDecimalPlaces {
            get;
            private set;
        }

        /// <summary>
        /// Gets the result column spacing.
        /// </summary>
        /// <value>The result column spacing.</value>
        public int ResultColumnSpacing {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the custom parameters.
        /// </summary>
        /// <value>The custom parameters.</value>
        public ICustomParameters CustomParameters {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the chart drawing properties.
        /// </summary>
        /// <value>The chart drawing properties.</value>
        public DrawingProperties ChartDrawingProperties {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the statistics formats.
        /// </summary>
        /// <value>The statistics formats.</value>
        public IDictionary<string, NumericValueDisplayFormat> StatisticsFormats {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the page break types.
        /// </summary>
        /// <value>The page break types.</value>
        public EnumDictionary<PageBreakType, bool> PageBreakTypes {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the chart scale.
        /// </summary>
        /// <value>The chart scale.</value>
        public double ChartScale {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the diagram style.
        /// </summary>
        /// <value>The diagram style.</value>
        public DiagramStyleType DiagramStyle {
            get;
            set;
        }
    }
}

