/*
 * The base library for ProbeNet print exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using FrankPti.Collections;

namespace ProbeNet.Export.Print
{
    /// <summary>
    /// Print parameter list.
    /// </summary>
    public class PrintParameterList: List<PrintParameter>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.Print.PrintParameterList"/> class.
        /// </summary>
        public PrintParameterList ()
        {
        }

        /// <summary>
        /// Gets or sets the nothing substitute.
        /// </summary>
        /// <value>The nothing substitute.</value>
        public string NothingSubstitute {
            get {
                return this[0].NothingSubstitute;
            }
            set {
                foreach (PrintParameter parameter in this) {
                    parameter.NothingSubstitute = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the print ranges.
        /// </summary>
        /// <value>The print ranges.</value>
        public EnumDictionary<PrintRange, bool> PrintRanges {
            get {
                return this[0].PrintRanges;
            }
            set {
                foreach (PrintParameter parameter in this) {
                    parameter.PrintRanges = value;
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is single.
        /// </summary>
        /// <value><c>true</c> if this instance is single; otherwise, <c>false</c>.</value>
        public bool IsSingle {
            get {
                return Count == 1;
            }
        }
    }
}

