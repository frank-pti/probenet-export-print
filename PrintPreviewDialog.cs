/*
 * The base library for ProbeNet print exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Frank.Widgets;
using Frank.Widgets.Dialogs;
using FrankPti.Collections;
using Gtk;
using Internationalization;
using InternationalizationUi;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;

namespace ProbeNet.Export.Print
{
    /// <summary>
    /// Print preview dialog.
    /// </summary>
    public class PrintPreviewDialog : CustomDialog
    {
        private PagePreviewWidget previewWidget;
        private PagePreviewControlWidget previewControlWidget;
        private Notebook notebook;
        private GeneralPrintSettingsWidget generalSettingsWidget;
        private PrintLayoutWidget printLayoutWidget;
        private PrinterList printers;

        private IExportInformation selectedPrintExportInformation;
        private PrintExport selectedPrintExport;

        private IList<MeasurementSetAdapter> measurements;
        private PrintParameter parameter;
        private SettingsProvider settingsProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.Print.PrintPreviewDialog"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="exportList">Export list.</param>
        /// <param name="preselectExport">Preselect export.</param>
        /// <param name="measurements">Measurements.</param>
        /// <param name="parameter">Parameter.</param>
        /// <param name="settingsProvider">Settings provider.</param>
        public PrintPreviewDialog(
            I18n i18n,
            Window parent,
            IList<IExportInformation> exportList,
            IExportInformation preselectExport,
            IList<MeasurementSetAdapter> measurements,
            PrintParameter parameter,
            SettingsProvider settingsProvider) :
            base(i18n, i18n.TrObject(Constants.UiDomain, "Print"), parent)
        {
            int windowWidth = 800;
            int windowHeight = 600;
            if (Screen.Width * 1.0 / Screen.Height > 4.0 / 3.0) {
                windowHeight = (int)(Screen.Height * 0.85);
                windowWidth = (int)(windowHeight * 4.0 / 3.0);
            } else {
                windowWidth = (int)(Screen.Width * 0.9);
                windowHeight = (int)(windowWidth * 3.0 / 4.0);
            }
            SetDefaultSize(windowWidth, windowHeight);
            this.measurements = measurements;
            this.parameter = parameter;
            this.settingsProvider = settingsProvider;

            PrintDocument printDoc = new PrintDocument();
            PrinterSettings printerSettings = printDoc.PrinterSettings;

            printers = new PrinterList();

            // SETTINGS
            IList<MeasurementAlignmentIdentifier> measurementAlignments = new List<MeasurementAlignmentIdentifier>();
            bool oneHasCurve = false;
            bool oneHasAttributes = false;
            bool oneHasParameters = false;

            if (measurements.Count == 1) {
                measurementAlignments = new List<MeasurementAlignmentIdentifier>(measurements[0].Measurements.Keys);
            }
            foreach (MeasurementSetAdapter adapter in measurements) {
                if (adapter.MeasurementDescription.CurveDescription != null) {
                    oneHasCurve = true;
                }
                if (adapter.MeasurementDescription.ParameterDescriptions != null) {
                    oneHasParameters = true;
                }
                if (adapter.Model.TextualAttributes != null) {
                    oneHasAttributes = true;
                }
            }

            generalSettingsWidget = new GeneralPrintSettingsWidget(
                i18n, printers.Printers, measurementAlignments, parameter.PrintRanges, parameter.PageBreakTypes,
                parameter.DiagramStyle, oneHasCurve, oneHasAttributes, oneHasParameters, measurements.Count);
            generalSettingsWidget.ChartScaleChanged += HandleGeneralSettingsWidgetChanged;
            generalSettingsWidget.MeasurementPageBreakChanged += HandleGeneralSettingsWidgetChanged;
            generalSettingsWidget.PrintRangesChanged += HandleGeneralSettingsWidgetChanged;
            generalSettingsWidget.PaperSizeChanged += HandleGeneralSettingsWidgetChanged;
            generalSettingsWidget.PaperOrientationChanged += HandleGeneralSettingsWidgetChanged;
            generalSettingsWidget.SampleAlignmentsChanged += HandleGeneralSettingsWidgetChanged;
            generalSettingsWidget.DiagramStyleChanged += HandleGeneralSettingsWidgetChanged;

            notebook = new Notebook();
            notebook.InsertPage(generalSettingsWidget,
                 new TranslatableLabel(i18n, Constants.UiDomain, "General"),
                 0
            );

            // PREVIEW
            previewWidget = new PagePreviewWidget();
            previewWidget.PageCountChanged += HandlePreviewWidgetPageCountChanged;
            previewControlWidget = new PagePreviewControlWidget(i18n, 1, 1);
            previewControlWidget.GoToPage += HandlePreviewControlWidgetGoToPage;

            // EXPORT TYPE SELECTION
            printLayoutWidget = new PrintLayoutWidget(i18n, exportList) {
                SelectedLayout = preselectExport
            };
            printLayoutWidget.DataExportChanged += HandleDataExportChanged;
            SelectedPrintExportInformation = printLayoutWidget.SelectedLayout;
            printLayoutWidget.NoShowAll = true;
            printLayoutWidget.Visible = (exportList.Count > 1);

            UpdatePreview();

            VBox previewBox = new VBox(false, 0);
            previewBox.PackStart(previewWidget, true, true, 3);
            previewBox.PackStart(previewControlWidget, false, true, 3);

            HPaned contentPaned = new HPaned();
            contentPaned.Pack1(previewBox, true, false);
            contentPaned.Pack2(notebook, true, false);

            VBox.PackStart(printLayoutWidget, false, false, 0);
            VBox.PackStart(contentPaned, true, true, 8);

            KeyValuePair<Button, ResponseType>[] buttonsList = BuildButtons(i18n);
            foreach (KeyValuePair<Button, ResponseType> pair in buttonsList) {
                AddButton(pair.Key, pair.Value);
            }
            Default = buttonsList[0].Key;
        }

        private void HandleDataExportChanged(object sender, EventArgs e)
        {
            SelectedPrintExportInformation = printLayoutWidget.SelectedLayout;
        }

        /// <summary>
        /// Gets the selected print export.
        /// </summary>
        /// <value>The selected print export.</value>
        public PrintExport SelectedPrintExport
        {
            get
            {
                return selectedPrintExport;
            }
            private set
            {
                selectedPrintExport = value;
                UpdatePreview();
            }
        }

        /// <summary>
        /// Gets or sets the selected print export information.
        /// </summary>
        /// <value>The selected print export information.</value>
        public IExportInformation SelectedPrintExportInformation
        {
            get
            {
                return selectedPrintExportInformation;
            }
            set
            {
                selectedPrintExportInformation = value;
                SelectedPrintExport = selectedPrintExportInformation.CreateExport<PrintExport>(settingsProvider);
            }
        }

        /// <summary>
        /// Gets the size of the paper.
        /// </summary>
        /// <value>The size of the paper.</value>
        public Gtk.PaperSize PaperSize
        {
            get
            {
                if (generalSettingsWidget == null) {
                    return null;
                }
                return generalSettingsWidget.SelectedPaperSize;
            }
        }

        /// <summary>
        /// Gets the page orientation.
        /// </summary>
        /// <value>The page orientation.</value>
        public PageOrientation PageOrientation
        {
            get
            {
                if (generalSettingsWidget == null) {
                    return Gtk.PageOrientation.Portrait;
                }
                return generalSettingsWidget.PaperOrientation;
            }
        }

        /// <summary>
        /// Gets the print ranges.
        /// </summary>
        /// <value>The print ranges.</value>
        public EnumDictionary<PrintRange, bool> PrintRanges
        {
            get
            {
                return generalSettingsWidget.PrintRanges;
            }
        }

        /// <summary>
        /// Gets the page break types.
        /// </summary>
        /// <value>The page break types.</value>
        public EnumDictionary<PageBreakType, bool> PageBreakTypes
        {
            get
            {
                return generalSettingsWidget.PageBreakTypes;
            }
        }

        private static KeyValuePair<Button, ResponseType>[] BuildButtons(I18n i18n)
        {
            return new KeyValuePair<Button, ResponseType>[] {
                BuildResponseButton (i18n.TrObject (Constants.UiDomain, "_Print"), Stock.Print, ResponseType.Accept),
                BuildResponseButton (i18n.TrObject (Constants.UiDomain, "_Cancel"), Stock.Cancel, ResponseType.Cancel)
            };
        }

        private void HandleGeneralSettingsWidgetChanged()
        {
            UpdatePreview();
        }

        private void HandlePreviewWidgetPageCountChanged()
        {
            previewControlWidget.PageCount = previewWidget.PageCount;
        }

        private void HandlePreviewControlWidgetGoToPage(int page)
        {
            previewWidget.CurrentPage = page - 1;
        }

        private void UpdateSettings()
        {
            if (SelectedPrintExportInformation != null && generalSettingsWidget != null) {
                parameter.PrintRanges = PrintRanges;
                parameter.PageBreakTypes = generalSettingsWidget.PageBreakTypes;
                parameter.ChartScale = generalSettingsWidget.ChartScale;
                parameter.DiagramStyle = generalSettingsWidget.DiagramStyle;
                IDictionary<MeasurementAlignmentIdentifier, bool> selectedAlignments = null;
                if (measurements.Count == 1) {
                    selectedAlignments = generalSettingsWidget.SelectedAlignmentIdentifiers;
                }
                SelectedPrintExport.BuildReportPainter(
                    i18n,
                    generalSettingsWidget.Paper,
                    measurements,
                    parameter,
                    selectedAlignments);
                int currentPage = previewWidget.CurrentPage;
                previewWidget.Painter = SelectedPrintExport.ReportPainter;
                previewWidget.CurrentPage = currentPage;
            }
        }

        private void UpdatePreview()
        {
            UpdateSettings();
            if (generalSettingsWidget != null) {
                if (generalSettingsWidget.SelectedAlignmentIdentifiers != null) {
                    CheckSelection(generalSettingsWidget.SelectedAlignmentIdentifiers.Count);
                }
            }
            if (previewWidget != null) {
                previewWidget.QueueDraw();
            }
        }

        private bool CheckSelection(int reportPainterCount)
        {
            bool success = true;
            bool atLeastOnePartSelected = false;
            foreach (bool partSelected in PrintRanges.Values) {
                atLeastOnePartSelected |= partSelected;
            }

            if (!atLeastOnePartSelected) {
                TranslationString message = i18n.TrObject(Constants.UiDomain,
                 "You must select at least one range option for printing!");
                DisplayAlert(message);
                success = false;
            } else {
                HideAlert();
                success = true;
            }
            return success;
        }

        private void DisplayAlert(TranslationString message)
        {
            DisplayAlert(message, ResponseType.Accept);
        }

        private void HideAlert()
        {
            HideAlert(ResponseType.Accept);
        }
    }
}

