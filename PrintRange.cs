/*
 * The base library for ProbeNet print exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.ComponentModel;
using Internationalization;

namespace ProbeNet.Export.Print
{
    /// <summary>
    /// Print range.
    /// </summary>
    public enum PrintRange
    {
        /// <summary>
        /// Print the attributes
        /// </summary>
        [Description("attributes")]
        [TranslatableCaption(Constants.UiDomain, "Attributes")]
        Attributes,

        /// <summary>
        /// Print the device information.
        /// </summary>
        [Description("device-information")]
        [TranslatableCaption(Constants.UiDomain, "Device information")]
        DeviceInformation,

        /// <summary>
        /// Print the custom measurement parameters.
        /// </summary>
        [Description("custom-measurement-parameters")]
        [TranslatableCaption(Constants.UiDomain, "Custom measurement parameters")]
        CustomMeasurementParameters,

        /// <summary>
        /// Print the parameters.
        /// </summary>
        [Description("parameters")]
        [TranslatableCaption(Constants.UiDomain, "Parameters")]
        Parameters,

        /// <summary>
        /// Print the measurement data.
        /// </summary>
        [Description("data")]
        [TranslatableCaption(Constants.UiDomain, "Data")]
        Data,

        /// <summary>
        /// Print the statistics.
        /// </summary>
        [Description("statistics")]
        [TranslatableCaption(Constants.UiDomain, "Statistics")]
        Statistics,

        /// <summary>
        /// Print the chart.
        /// </summary>
        [Description("chart")]
        [TranslatableCaption(Constants.UiDomain, "Chart")]
        Chart
    }

}

