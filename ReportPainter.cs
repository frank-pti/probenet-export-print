using Frank.Widgets.Model;
/*
 * The base library for ProbeNet print exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace ProbeNet.Export.Print
{
    /// <summary>
    /// Base class for report painters.
    /// </summary>
	public abstract class ReportPainter
	{
        /// <summary>
        /// Delegate method for changed events.
        /// </summary>
		public delegate void ChangedDelegate();
        /// <summary>
        /// Occurs when paper changed.
        /// </summary>
		public event ChangedDelegate PaperChanged;

        /// <summary>
        /// Delegate method for context changed event.
        /// </summary>
        public delegate void ContextChangedDelegate(Cairo.Context context);
        /// <summary>
        /// Occurs when context changed.
        /// </summary>
        public event ContextChangedDelegate ContextChanged;

		private PaperInformation paper;
        private Cairo.Context context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.Print.ReportPainter"/> class.
        /// </summary>
        /// <param name="paper">Paper.</param>
		public ReportPainter(PaperInformation paper)
		{
			this.paper = paper;
		}

        /// <summary>
        /// Gets the amount of pages needed to paint the whole report.
        /// </summary>
        /// <value>The pages.</value>
		public abstract int Pages {
			get;
		}

        /// <summary>
        /// Gets or sets the paper.
        /// </summary>
        /// <value>The paper.</value>
		public PaperInformation Paper {
			get {
				return paper;
			}
			set {
				paper = value;
				NotifyPaperChanged();
			}
		}

        /// <summary>
        /// Gets or sets a value indicating whether a scalable part should scale only to
        /// keep it on the same page.
        /// </summary>
        /// <value><c>true</c> if scale only to keep scalable on page; otherwise, <c>false</c>.</value>
        public bool ScaleOnlyToKeepScalableOnPage {
            get;
            set;
        }

		private void NotifyPaperChanged()
		{
			if (PaperChanged != null) {
				PaperChanged();
			}
		}

        /// <summary>
        /// Gets or sets the context.
        /// </summary>
        /// <value>The context.</value>
        public Cairo.Context Context {
            get {
                return context;
            }
            set {
                if (context != value) {
                    context = value;
                    NotifyContextChanged();
                }
            }
        }

        private void NotifyContextChanged ()
        {
            if (ContextChanged != null) {
                ContextChanged(Context);
            }
        }

        /// <summary>
        /// Calculates the pages that are needed to paint the whole report.
        /// </summary>
        /// <returns>The amount of pages needed to paint the whole report.</returns>
        /// <param name="context">Context.</param>
		public abstract int CalculatePages(Cairo.Context context);

        /// <summary>
        /// Paint the specified page using the specified context.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="currentPage">Current page.</param>
		public abstract void Paint(Cairo.Context context, int currentPage);
	}
}

